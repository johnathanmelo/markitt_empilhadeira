﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using OnBarcode.Barcode.BarcodeScanner;
using System.Threading;
using System.Net;
using Proshot.CommandClient;
using BarcodeLib.BarcodeReader;

namespace markitt_empilhadeira
{
    public partial class MainWindow : Form
    {
        // Video and Scan Code
        AForge.Video.DirectShow.VideoCaptureDevice videoSource1;
        AForge.Video.DirectShow.VideoCaptureDevice videoSource2;
        string fileTitle1 = "snapshot1.png";
        string fileTitle2 = "snapshot2.png";
        protected String[] barcodes1;
        protected String[] barcodes2;

        // Communication
        private Proshot.CommandClient.CMDClient client;
        IPAddress ipServer = IPAddress.Parse("127.0.0.1");

        public MainWindow()
        {            
            InitializeComponent();
            StartLogin();
            InitializeCameras();
            StartScanning();
           
        }

        private void StartLogin()
        {
            this.client = new CMDClient(ipServer, 8000, "Empilhadeira");
            this.client.CommandReceived += new CommandReceivedEventHandler(client_CommandReceived);

            // Start timerLogion
            timerLogin.Enabled = true;
            timerLogin.Interval = 5000;
            timerLogin.Tick += new EventHandler(this.timerLogin_Tick);           
        }
        
        private void InitializeCameras()
        {
            AForge.Video.DirectShow.FilterInfoCollection videosources = new AForge.Video.DirectShow.FilterInfoCollection(AForge.Video.DirectShow.FilterCategory.VideoInputDevice);

            if (videosources != null)
            {
                // Para alterar qual câmera usar, basta alterar o índice i de videosources[i].MonikerString

                // First Camera
                videoSource1 = new AForge.Video.DirectShow.VideoCaptureDevice(videosources[1].MonikerString);
                videoSource1.NewFrame += (s, e) => webCamPictureBox1.Image = (Bitmap)e.Frame.Clone();
                videoSource1.Start();

                // Second Camera
                videoSource2 = new AForge.Video.DirectShow.VideoCaptureDevice(videosources[4].MonikerString);
                videoSource2.NewFrame += (s, e) => webCamPictureBox2.Image = (Bitmap)e.Frame.Clone();
                videoSource2.Start();

                Thread.Sleep(7000);                
            }
        }

        private void StartScanning()
        {
            // Start timer1
            timer1.Enabled = true;
            timer1.Interval = 100;
            timer1.Tick += new System.EventHandler(this.timer1_Tick);
            Thread.Sleep(50);

            // Start timer2
            timer2.Enabled = true;
            timer2.Interval = 100;
            timer2.Tick += new EventHandler(this.timer2_Tick);

        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            this.client.Disconnect();

            if (videoSource1 != null && videoSource1.IsRunning)
            {
                videoSource1.SignalToStop();
                videoSource1 = null;
            }

            if (videoSource2 != null && videoSource2.IsRunning)
            {
                videoSource2.SignalToStop();
                videoSource2 = null;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (videoSource1.IsRunning && webCamPictureBox1.Image != null)
            {
                webCamPictureBox1.Image.Save(fileTitle1, System.Drawing.Imaging.ImageFormat.Png);

                Bitmap imageFrame = (Bitmap)Image.FromFile(fileTitle1, true);

                //barcodes1 = BarcodeScanner.ScanSingleBarcode(imageFrame, BarcodeType.QRCode);
                barcodes1 = BarcodeReader.read(imageFrame, BarcodeReader.QRCODE);

                if (barcodes1 != null)
                {
                    // enviar código para o servidor com flag automática
                    if (this.client.Connected)
                    {
                        // remove primeiro caractere da string
                        string qrcode = barcodes1.First();
                        qrcode = qrcode.Substring(1, qrcode.Length - 1);
                        this.client.SendCommand(new Command(Proshot.CommandClient.CommandType.QRCodeAuto, ipServer, qrcode));
                    }
                    else
                    {
                        this.timerLogin.Enabled = true;
                    }
                }

                imageFrame.Dispose();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (videoSource2.IsRunning && webCamPictureBox2.Image != null)
            {
                webCamPictureBox2.Image.Save(fileTitle2, System.Drawing.Imaging.ImageFormat.Png);

                Bitmap imageFrame = (Bitmap)Image.FromFile(fileTitle2, true);

                //barcodes2 = BarcodeScanner.ScanSingleBarcode(imageFrame, BarcodeType.QRCode);
                barcodes2 = BarcodeReader.read(imageFrame, BarcodeReader.QRCODE);

                if (barcodes2 != null)
                {
                    // enviar código para o servidor com flag automática
                    if (this.client.Connected)
                    {
                        // remove primeiro caractere da string
                        string qrcode = barcodes2.First();
                        qrcode = qrcode.Substring(1, qrcode.Length-1);
                        this.client.SendCommand(new Proshot.CommandClient.Command(Proshot.CommandClient.CommandType.QRCodeAuto, ipServer, qrcode));
                    }
                    else
                    {
                        this.timerLogin.Enabled = true;
                    }
                }

                imageFrame.Dispose();
            }
        }

        private void buttonManual_Click(object sender, EventArgs e)
        {
            labelInstruction.Visible = true;
            textBoxManualCode.Visible = true;
            buttonSend.Visible = true;
            buttonManual.Visible = false;
        }        

        private void buttonSend_Click(object sender, EventArgs e)
        {
            // enviar código para servidor com flag manual
            if (this.client.Connected)
            {
                if (textBoxManualCode.Text != "")
                   this.client.SendCommand(new Command(CommandType.QRCodeManual, ipServer, textBoxManualCode.Text));                
            }
            else 
            {
                this.timerLogin.Enabled = true;
            }

            textBoxManualCode.Clear();
            labelInstruction.Visible = false;
            textBoxManualCode.Visible = false;
            buttonSend.Visible = false;
            buttonManual.Visible = true;
        }

        void client_CommandReceived(object sender, CommandEventArgs e)
        {
            switch (e.Command.CommandType)
            {
                case (CommandType.Message):
                    textBoxMessage.Text = e.Command.MetaData;
                    break;

                case (CommandType.CodeVerificationResult):

                    if(e.Command.MetaData == "BagCorreta")
                    {
                        checkBoxBag.Checked = true;
                        textBoxWarning.Text = "";
                    }
                    else if(e.Command.MetaData == "BagErrada")
                    {
                        textBoxWarning.Text = "BAG INCORRETA";
                    }
                    else if(e.Command.MetaData == "LocalCorreto")
                    {
                        checkBoxLocal.Checked = true;
                        textBoxWarning.Text = "";
                    }
                    else if (e.Command.MetaData == "LocalErrado")
                    {
                        textBoxWarning.Text = "LOCAL INCORRETO";
                    }
                    break;

                case (CommandType.OperationConclusion):
                    checkBoxBag.Checked = false;
                    checkBoxLocal.Checked = false;
                    textBoxWarning.Clear();
                    textBoxMessage.Text = "Aguardando próxima operação...";
                    break;
            }
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            if (this.client.Connected == false)
            {
                textBoxMessage.Text = "Conectando com servidor...";
                buttonSend.Enabled = false;
                this.client.ConnectToServer();
            }
            else 
            {
                textBoxMessage.Clear();
                buttonSend.Enabled = true;
                this.timerLogin.Enabled = false;
            }
        }
        
        private void buttonShowCameras_Click(object sender, EventArgs e)
        {
            // Camera Items
            webCamPictureBox1.Visible = true;
            webCamPictureBox2.Visible = true;
            labelCam1.Visible = true;
            labelCam2.Visible = true;

            // Map Items
            mapPictureBox.Visible = false;
        }

        private void buttonShowMap_Click(object sender, EventArgs e)
        {
            // Camera Items
            webCamPictureBox1.Visible = false;
            webCamPictureBox2.Visible = false;
            labelCam1.Visible = false;
            labelCam2.Visible = false;

            // Map Items
            mapPictureBox.Visible = true;
        }

        private void textBoxManualCode_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxManualCode.Text))
            {
                buttonSend.Text = "Voltar";
            }
            else
            {
                buttonSend.Text = "Enviar";
            }
        }
    }
}
