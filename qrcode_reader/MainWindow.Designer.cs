﻿namespace markitt_empilhadeira
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelCam1 = new System.Windows.Forms.Label();
            this.labelCam2 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.textBoxManualCode = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.labelInstruction = new System.Windows.Forms.Label();
            this.timerLogin = new System.Windows.Forms.Timer(this.components);
            this.checkBoxBag = new System.Windows.Forms.CheckBox();
            this.checkBoxLocal = new System.Windows.Forms.CheckBox();
            this.textBoxWarning = new System.Windows.Forms.TextBox();
            this.mapPictureBox = new System.Windows.Forms.PictureBox();
            this.buttonShowMap = new System.Windows.Forms.Button();
            this.buttonShowCameras = new System.Windows.Forms.Button();
            this.buttonManual = new System.Windows.Forms.Button();
            this.webCamPictureBox2 = new System.Windows.Forms.PictureBox();
            this.webCamPictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.mapPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCam1
            // 
            this.labelCam1.AutoSize = true;
            this.labelCam1.Location = new System.Drawing.Point(305, 155);
            this.labelCam1.Name = "labelCam1";
            this.labelCam1.Size = new System.Drawing.Size(36, 13);
            this.labelCam1.TabIndex = 4;
            this.labelCam1.Text = "CAM1";
            // 
            // labelCam2
            // 
            this.labelCam2.AutoSize = true;
            this.labelCam2.Location = new System.Drawing.Point(1018, 155);
            this.labelCam2.Name = "labelCam2";
            this.labelCam2.Size = new System.Drawing.Size(36, 13);
            this.labelCam2.TabIndex = 5;
            this.labelCam2.Text = "CAM2";
            // 
            // textBoxManualCode
            // 
            this.textBoxManualCode.Location = new System.Drawing.Point(1075, 68);
            this.textBoxManualCode.Name = "textBoxManualCode";
            this.textBoxManualCode.Size = new System.Drawing.Size(195, 20);
            this.textBoxManualCode.TabIndex = 8;
            this.textBoxManualCode.Visible = false;
            this.textBoxManualCode.TextChanged += new System.EventHandler(this.textBoxManualCode_TextChanged);
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(1276, 29);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(75, 75);
            this.buttonSend.TabIndex = 9;
            this.buttonSend.Text = "Voltar";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Visible = false;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMessage.Location = new System.Drawing.Point(422, 29);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(517, 44);
            this.textBoxMessage.TabIndex = 10;
            this.textBoxMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelInstruction
            // 
            this.labelInstruction.AutoSize = true;
            this.labelInstruction.Location = new System.Drawing.Point(1072, 43);
            this.labelInstruction.Name = "labelInstruction";
            this.labelInstruction.Size = new System.Drawing.Size(198, 13);
            this.labelInstruction.TabIndex = 11;
            this.labelInstruction.Text = "DIGITE o código e CLIQUE em ENVIAR";
            this.labelInstruction.Visible = false;
            // 
            // checkBoxBag
            // 
            this.checkBoxBag.AutoCheck = false;
            this.checkBoxBag.AutoSize = true;
            this.checkBoxBag.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBag.Location = new System.Drawing.Point(528, 113);
            this.checkBoxBag.Name = "checkBoxBag";
            this.checkBoxBag.Size = new System.Drawing.Size(68, 28);
            this.checkBoxBag.TabIndex = 14;
            this.checkBoxBag.Text = "BAG";
            this.checkBoxBag.UseVisualStyleBackColor = true;
            // 
            // checkBoxLocal
            // 
            this.checkBoxLocal.AutoCheck = false;
            this.checkBoxLocal.AutoSize = true;
            this.checkBoxLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxLocal.Location = new System.Drawing.Point(781, 113);
            this.checkBoxLocal.Name = "checkBoxLocal";
            this.checkBoxLocal.Size = new System.Drawing.Size(72, 28);
            this.checkBoxLocal.TabIndex = 15;
            this.checkBoxLocal.Text = "PISO";
            this.checkBoxLocal.UseVisualStyleBackColor = true;
            // 
            // textBoxWarning
            // 
            this.textBoxWarning.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxWarning.Location = new System.Drawing.Point(619, 119);
            this.textBoxWarning.Name = "textBoxWarning";
            this.textBoxWarning.Size = new System.Drawing.Size(135, 20);
            this.textBoxWarning.TabIndex = 16;
            this.textBoxWarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mapPictureBox
            // 
            this.mapPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mapPictureBox.BackgroundImage")));
            this.mapPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.mapPictureBox.Location = new System.Drawing.Point(299, 196);
            this.mapPictureBox.Name = "mapPictureBox";
            this.mapPictureBox.Size = new System.Drawing.Size(789, 461);
            this.mapPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.mapPictureBox.TabIndex = 19;
            this.mapPictureBox.TabStop = false;
            this.mapPictureBox.Visible = false;
            // 
            // buttonShowMap
            // 
            this.buttonShowMap.BackgroundImage = global::markitt_empilhadeira.Properties.Resources.map;
            this.buttonShowMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonShowMap.Location = new System.Drawing.Point(129, 26);
            this.buttonShowMap.Name = "buttonShowMap";
            this.buttonShowMap.Size = new System.Drawing.Size(75, 75);
            this.buttonShowMap.TabIndex = 18;
            this.buttonShowMap.UseVisualStyleBackColor = true;
            this.buttonShowMap.Click += new System.EventHandler(this.buttonShowMap_Click);
            // 
            // buttonShowCameras
            // 
            this.buttonShowCameras.BackgroundImage = global::markitt_empilhadeira.Properties.Resources.cam_icon;
            this.buttonShowCameras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonShowCameras.Location = new System.Drawing.Point(18, 25);
            this.buttonShowCameras.Name = "buttonShowCameras";
            this.buttonShowCameras.Size = new System.Drawing.Size(75, 75);
            this.buttonShowCameras.TabIndex = 17;
            this.buttonShowCameras.UseVisualStyleBackColor = true;
            this.buttonShowCameras.Click += new System.EventHandler(this.buttonShowCameras_Click);
            // 
            // buttonManual
            // 
            this.buttonManual.BackgroundImage = global::markitt_empilhadeira.Properties.Resources.warning_icon_hi;
            this.buttonManual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonManual.Location = new System.Drawing.Point(1276, 29);
            this.buttonManual.Name = "buttonManual";
            this.buttonManual.Size = new System.Drawing.Size(75, 75);
            this.buttonManual.TabIndex = 6;
            this.buttonManual.UseVisualStyleBackColor = true;
            this.buttonManual.Click += new System.EventHandler(this.buttonManual_Click);
            // 
            // webCamPictureBox2
            // 
            this.webCamPictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.webCamPictureBox2.Location = new System.Drawing.Point(691, 178);
            this.webCamPictureBox2.Name = "webCamPictureBox2";
            this.webCamPictureBox2.Size = new System.Drawing.Size(660, 479);
            this.webCamPictureBox2.TabIndex = 3;
            this.webCamPictureBox2.TabStop = false;
            // 
            // webCamPictureBox1
            // 
            this.webCamPictureBox1.Location = new System.Drawing.Point(18, 178);
            this.webCamPictureBox1.Name = "webCamPictureBox1";
            this.webCamPictureBox1.Size = new System.Drawing.Size(660, 479);
            this.webCamPictureBox1.TabIndex = 2;
            this.webCamPictureBox1.TabStop = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 694);
            this.Controls.Add(this.mapPictureBox);
            this.Controls.Add(this.buttonShowMap);
            this.Controls.Add(this.buttonShowCameras);
            this.Controls.Add(this.textBoxWarning);
            this.Controls.Add(this.checkBoxLocal);
            this.Controls.Add(this.checkBoxBag);
            this.Controls.Add(this.labelInstruction);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.textBoxManualCode);
            this.Controls.Add(this.buttonManual);
            this.Controls.Add(this.labelCam2);
            this.Controls.Add(this.labelCam1);
            this.Controls.Add(this.webCamPictureBox2);
            this.Controls.Add(this.webCamPictureBox1);
            this.Name = "MainWindow";
            this.Text = "Empilhadeira";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mapPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webCamPictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox webCamPictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox webCamPictureBox2;
        private System.Windows.Forms.Label labelCam1;
        private System.Windows.Forms.Label labelCam2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button buttonManual;
        private System.Windows.Forms.TextBox textBoxManualCode;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label labelInstruction;
        private System.Windows.Forms.Timer timerLogin;
        private System.Windows.Forms.CheckBox checkBoxBag;
        private System.Windows.Forms.CheckBox checkBoxLocal;
        private System.Windows.Forms.TextBox textBoxWarning;
        private System.Windows.Forms.Button buttonShowCameras;
        private System.Windows.Forms.Button buttonShowMap;
        private System.Windows.Forms.PictureBox mapPictureBox;
    }
}

